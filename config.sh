# LibreWolf https://librewolf.net/installation/debian/
echo "deb [arch=amd64] http://deb.librewolf.net $(lsb_release -sc) main" | sudo tee /etc/apt/sources.list.d/librewolf.list
sudo wget https://deb.librewolf.net/keyring.gpg -O /etc/apt/trusted.gpg.d/librewolf.gpg
# VSCodium https://vscodium.com/#install
wget -qO - https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg \
    | gpg --dearmor \
    | sudo dd of=/usr/share/keyrings/vscodium-archive-keyring.gpg
echo 'deb [ signed-by=/usr/share/keyrings/vscodium-archive-keyring.gpg ] https://download.vscodium.com/debs vscodium main' \
| sudo tee /etc/apt/sources.list.d/vscodium.list
# Typora
wget -qO - https://typora.io/linux/public-key.asc | sudo apt-key add -
sudo add-apt-repository 'deb https://typora.io/linux ./'

sudo apt update && sudo apt install codium librewolf typora

mkdir ~/{Backup,Keepass,Notes}
rsync -av -e "ssh -p 53178" pop-os-rsync@192.168.1.22::XDrive/daily/{Backup,Keepass,Notes} ~/

cp ~/Backup/librewolf-browser-profile.tar.bz2 ~/
rm ~rf ~/.librewolf
cd ~/
tar -xvf librewolf-browser-profile.tar.bz2
rm librewolf-browser-profile.tar.bz2